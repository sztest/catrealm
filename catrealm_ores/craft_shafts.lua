-- LUALOCALS < ---------------------------------------------------------
local minetest, nodecore
    = minetest, nodecore
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local function reg(material, lm, prillname, sounds, anvilfunc)
	minetest.register_node(modname .. ":" .. lm .. "_bar", {
			description = material .. " Bar",
			drawtype = "nodebox",
			node_box = nodecore.fixedbox(-1/16, -0.5, -1/16, 1/16, 0, 1/16),
			selection_box = nodecore.fixedbox(-1/8, -0.5, -1/8, 1/8, 0, 1/8),
			tiles = {"catrealm_ores_" .. lm .. ".png"},
			crush_damage = 1,
			paramtype = "light",
			sunlight_propagates = true,
			groups = {
				cracky = 2,
				falling_repose = 1,
				chisel = 1
			},
			sounds = sounds,
			mapcolor = {a = 0},
		})

	anvilfunc(-1, {
			label = "anvil making " .. lm .. " bar",
			priority = -1,
			action = "pummel",
			toolgroups = {thumpy = 3},
			indexkeys = {modname .. ":" .. lm .. "_" .. prillname},
			nodes = {
				{
					match = modname .. ":" .. lm .. "_" .. prillname,
					replace = "air"
				}
			},
			items = {
				modname .. ":" .. lm .. "_bar"
			}
		})

	anvilfunc(-1, {
			label = "anvil recycle " .. lm .. " bar",
			priority = -1,
			action = "pummel",
			toolgroups = {thumpy = 3},
			normal = {y = 1},
			indexkeys = {modname .. ":" .. lm .. "_bar"},
			nodes = {
				{
					match = modname .. ":" .. lm .. "_bar",
					replace = "air"
				}
			},
			items = {
				modname .. ":" .. lm .. "_" .. prillname
			}
		})

	minetest.register_node(modname .. ":" .. lm .. "_rod", {
			description = material .. " Rod",
			drawtype = "nodebox",
			node_box = nodecore.fixedbox(-1/16, -0.5, -1/16, 1/16, 0.5, 1/16),
			selection_box = nodecore.fixedbox(-1/8, -0.5, -1/8, 1/8, 0.5, 1/8),
			tiles = {"catrealm_ores_" .. lm .. ".png"},
			crush_damage = 2,
			paramtype = "light",
			sunlight_propagates = true,
			groups = {
				cracky = 2,
				falling_repose = 2,
				chisel = 2
			},
			sounds = sounds,
			mapcolor = {a = 0},
		})

	anvilfunc(-2, {
			label = "anvil making " .. lm .. " rod",
			action = "pummel",
			toolgroups = {thumpy = 3},
			indexkeys = {modname .. ":" .. lm .. "_bar"},
			nodes = {
				{
					match = {name = modname .. ":" .. lm .. "_bar"},
					replace = "air"
				},
				{
					y = -1,
					match = {name = modname .. ":" .. lm .. "_bar"},
					replace = "air"
				}
			},
			items = {
				modname .. ":" .. lm .. "_rod"
			}
		})

	anvilfunc(-1, {
			label = "recycle " .. lm .. " rod",
			action = "pummel",
			toolgroups = {choppy = 3},
			indexkeys = {modname .. ":" .. lm .. "_rod"},
			nodes = {
				{
					match = modname .. ":" .. lm .. "_rod",
					replace = "air"
				}
			},
			items = {
				{name = modname .. ":" .. lm .. "_bar", count = 2}
			}
		})
end

local function chain(func, oldfunc)
	if not oldfunc then return func end
	return function(...)
		local r = func(...)
		if not r then return r end
		return oldfunc(...)
	end
end

reg("Sterling", "silver", "coin", nodecore.sounds("nc_lode_tempered", nil, 2), function(_, recipe)
		nodecore.register_craft(recipe)
	end)
reg("Tuxzite", "tuxzite", "prill", nodecore.sounds("nc_cats_mew", nil, 0.75), function(dy, recipe)
		recipe.check = chain(function(_, data)
				local pos = data.rel(0, dy, 0)
				local node = minetest.get_node(pos)
				return node.name == modname .. ":silver"
			end,
			recipe.check)
		nodecore.register_craft(recipe)
	end)
