-- LUALOCALS < ---------------------------------------------------------
local ipairs, math, minetest, nodecore, pairs, vector
    = ipairs, math, minetest, nodecore, pairs, vector
local math_random
    = math.random
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local catgroup = "nc_cats_cat"
local fullgroup = "group:" .. catgroup
local radius = 5

local function addstack(qty, stack)
	if minetest.get_item_group(stack:get_name(), catgroup) ~= 0 then
		return qty + stack:get_count()
	end
	return qty
end

minetest.register_abm({
		label = "tuxzite purring",
		interval = 2,
		chance = 2,
		nodenames = {modname .. ":tuxzite_ore"},
		action = function(pos)
			-- if exposed to air, no more purring
			for _, dir in ipairs(nodecore.dirs()) do
				if nodecore.air_equivalent(vector.add(pos, dir)) then
					return
				end
			end

			-- count nearby cats
			local cats = #nodecore.find_nodes_around(pos, fullgroup, radius)
			for _, npos in ipairs(nodecore.find_nodes_around(pos, "group:visinv", radius)) do
				cats = addstack(cats, nodecore.stack_get(npos))
			end
			for _, player in ipairs(minetest.get_connected_players()) do
				local ppos = player:get_pos()
				ppos.y = ppos.y + player:get_properties().eye_height
				if vector.distance(pos, ppos) <= radius then
					for _, item in pairs(player:get_inventory():get_list("main")) do
						cats = addstack(cats, item)
					end
				end
			end
			if cats < 1 then return end

			-- probability of purring
			local nopurr = 0.9 ^ cats
			if math_random() < nopurr then return end
			local n = math_random(1, 4)
			return nodecore.sound_play("nc_cats_purr_" .. n, {
					pitch = 0.75,
					pos = pos,
					max_hear_distance = radius * 2,
					ephemeral = true,
				})
		end
	})
