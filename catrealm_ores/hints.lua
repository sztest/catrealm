-- LUALOCALS < ---------------------------------------------------------
local minetest, nodecore
    = minetest, nodecore
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

-- ore_silver
nodecore.register_hint("find sterling ore",
	modname .. ":silver_ore",
	"catrealm teleport"
)
nodecore.register_hint("dig up sterling ore",
	"inv:" .. modname .. ":silver_cobble_loose",
	modname .. ":silver_ore"
)

-- ore_tuxzite
nodecore.register_hint("find tuxzite ore",
	modname .. ":tuxzite_ore",
	"catrealm teleport"
)
nodecore.register_hint("dig up tuxzite ore",
	"inv:" .. modname .. ":tuxzite_cobble_loose",
	modname .. ":tuxzite_ore"
)

-- craft_basic_silver
nodecore.register_hint("extract sterling from ore",
	{true, "extract silver from loose silver cobble", "extract silver from silver cobble"},
	modname .. ":silver_cobble_loose"
)
nodecore.register_hint("sinter sterling medallions into cube",
	"pack silver cube",
	modname .. ":silver_coin"
)
nodecore.register_hint("chop sterling cube into medallions",
	"unpack silver cube",
	"pack silver cube"
)

-- craft_basic_tuxzite
nodecore.register_hint("cook tuxzite out of cobble with a cat prill",
	"tuxzite extraction",
	modname .. ":tuxzite_cobble_loose"
)
nodecore.register_hint("pound tuxzite prill back into cobble",
	"tuxzite injection",
	modname .. ":tuxzite_prill"
)
nodecore.register_hint("sinter tuxzite prills into cube",
	"pack tuxzite cube",
	modname .. ":tuxzite_prill"
)
nodecore.register_hint("chop tuxzite cube into prills",
	"unpack tuxzite cube",
	"pack tuxzite cube"
)

-- craft_shafts
nodecore.register_hint("forge a sterling medallion into a bar",
	"anvil making silver bar",
	modname .. ":silver_coin"
)
nodecore.register_hint("forge sterling bars into a rod",
	"anvil making silver rod",
	"anvil making silver bar"
)
nodecore.register_hint("forge a tuxzite prill into a bar",
	"anvil making tuxzite bar",
	modname .. ":tuxzite_prill"
)
nodecore.register_hint("forge tuxzite bars into a rod",
	"anvil making tuxzite rod",
	"anvil making tuxzite bar"
)

-- craft_tools
nodecore.register_hint("assemble a tuxzite adze",
	"assemble tuxzite adze",
	{modname .. ":tuxzite_rod", modname .. ":tuxzite_bar"}
)
nodecore.register_hint("assemble a tuxzite rake",
	"assemble tuxzite rake",
	"assemble tuxzite adze"
)
nodecore.register_hint("forge tuxzite prills into a tool head",
	"forge tuxzite mallet_head",
	modname .. ":tuxzite_prill"
)
nodecore.register_hint("assemble a tuxzite tool with a sterling handle",
	{true,
		"assemble tuxzite mallet",
		"assemble tuxzite mace",
		"assemble tuxzite spade",
		"assemble tuxzite hatchet",
		"assemble tuxzite pick",
		"assemble tuxzite cattock",
	},
	"forge tuxzite mallet_head"
)
nodecore.register_hint("weld tuxzite pick and spade heads together",
	"weld tuxzite cattock",
	"forge tuxzite pick_head"
)

-- renew_silver
nodecore.register_hint("cook a tuxzite prill down to sterling",
	"silver renewal",
	modname .. ":tuxzite_prill"
)

-- renew_tuxzite
nodecore.register_hint("convert cat prills to tuxzite on a tuxzite cube",
	"tuxzite renewal",
	modname .. ":tuxzite"
)

-- rule_catrecharge
nodecore.register_hint("recharge a tuxzite tool",
	"cat recharging",
	{true,
		"assemble tuxzite mallet",
		"assemble tuxzite mace",
		"assemble tuxzite spade",
		"assemble tuxzite hatchet",
		"assemble tuxzite pick",
		"assemble tuxzite cattock",
	}
)
