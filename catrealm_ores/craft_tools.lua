-- LUALOCALS < ---------------------------------------------------------
local minetest, nodecore, pairs, string
    = minetest, nodecore, pairs, string
local string_lower
    = string.lower
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local function wear_to_plus_silver(item, coins)
	if (not coins) or coins < 1 then return item end
	return function(_, who)
		local left = who:get_inventory():add_item("main", modname .. ":silver_coin " .. coins)
		if not left:is_empty() then
			nodecore.item_eject(who:get_pos(), left)
		end
		return item
	end
end

local function mkonrake(toolcaps)
	local loosevol = nodecore.rake_volume(2, 1)
	local loosetest = nodecore.rake_index(function(def)
			return def.groups and def.groups.falling_node
			and def.groups.snappy == 1
		end)
	local snapvol = nodecore.rake_volume(1, 1)
	local snaptest = nodecore.rake_index(function(def)
			return def.groups and def.groups.snappy
			and def.groups.snappy <= toolcaps.opts.snappy
		end)
	local crumbvol = nodecore.rake_volume(1, 1)
	local crumbtest = nodecore.rake_index(function(def)
			return def.groups and def.groups.crumbly
			and def.groups.crumbly <= toolcaps.opts.crumbly
		end)
	local crackvol = nodecore.rake_volume(1, 1)
	local cracktest = nodecore.rake_index(function(def)
			return def.groups and def.groups.cracky
			and def.groups.cracky <= toolcaps.opts.cracky
		end)
	return function(pos, node)
		if loosetest(pos, node) then return loosevol, loosetest end
		if snaptest(pos, node) then return snapvol, snaptest end
		if crumbtest(pos, node) then return crumbvol, crumbtest end
		if cracktest(pos, node) then return crackvol, cracktest end
	end
end

local function mktool(name, itemgroups, toolcaps, prills, coins, israke, headitem)
	local lcname = string_lower(name)
	local headimg = "catrealm_ores_tuxzite.png^[mask:catrealm_ores_tool_" .. lcname .. ".png"
	local toolimg = headimg
	toolcaps = nodecore.toolcaps(toolcaps)
	if coins and coins > 0 then
		toolimg = toolimg .. "^(catrealm_ores_silver.png^[mask:catrealm_ores_tool_handle.png)"
		if not headitem then
			headitem = modname .. ":tuxzite_" .. lcname .. "_head"
			minetest.register_craftitem(headitem, {
					description = "Tuxzite " .. name .. " Head",
					wield_image = headimg,
					inventory_image = headimg,
					sounds = nodecore.sounds("nc_cats_mew", nil, 0.75),
					stack_max = 1,
					tool_head_capabilities = toolcaps,
				})
		end
		nodecore.register_craft({
				label = "assemble tuxzite " .. lcname,
				normal = {y = 1},
				indexkeys = {headitem},
				nodes = {
					{match = headitem, replace = "air"},
					{y = -1, match = modname .. ":silver_rod", replace = "air"},
				},
				items = {
					{y = -1, name = modname .. ":tuxzite_" .. lcname},
				}
			})
	end
	minetest.register_tool(modname .. ":tuxzite_" .. lcname, {
			description = "Tuxzite " .. name,
			wield_image = toolimg,
			inventory_image = toolimg,
			sounds = nodecore.sounds("nc_cats_mew", nil, 0.75),
			groups = itemgroups,
			tool_capabilities = toolcaps,
			tool_wears_to = wear_to_plus_silver(modname .. ":tuxzite_prill " .. prills, coins),
			on_rake = israke and mkonrake(toolcaps) or nil,
		})
end

mktool("Mallet", {catrealm_cat_recharge = 2500}, {uses = 0.00005, thumpy = 9}, 3, 2)
mktool("Mace", {catrealm_cat_recharge = 1000}, {
		uses = 0.0002,
		thumpy = 9,
		cracky = 9,
		crumbly = 9,
		choppy = 9
	}, 8, 2, nil, modname .. ":tuxzite")
mktool("Spade", {catrealm_cat_recharge = 2500}, {uses = 0.00005, crumbly = 9}, 2, 2)
mktool("Hatchet", {catrealm_cat_recharge = 2500}, {uses = 0.00005, choppy = 9}, 2, 2)
mktool("Pick", {catrealm_cat_recharge = 2500}, {uses = 0.00005, cracky = 9}, 1, 2)
mktool("Cattock", {catrealm_cat_recharge = 1500}, {uses = 0.0001, cracky = 9, crumbly = 9}, 3, 2)
mktool("Adze", {catrealm_cat_recharge = 3500}, {uses = 0.0001, choppy = 8, crumbly = 8, cracky = 6}, 3)
mktool("Rake", {
		catrealm_cat_recharge = 1500,
		rakey = 1,
		nc_doors_pummel_first = 1,
		}, {
		snappy = 1,
		crumbly = 7,
		cracky = 5,
		uses = 0.01,
		usesmin = 25
	}, 11, 0, true)

nodecore.register_craft({
		label = "assemble tuxzite adze",
		normal = {y = 1},
		indexkeys = {modname .. ":tuxzite_bar"},
		nodes = {
			{match = modname .. ":tuxzite_bar", replace = "air"},
			{y = -1, match = modname .. ":tuxzite_rod", replace = "air"},
		},
		items = {
			{y = -1, name = modname .. ":tuxzite_adze"}
		}
	})

local adze = {name = modname .. ":tuxzite_adze", wear = 0.05}
nodecore.register_craft({
		label = "assemble tuxzite rake",
		indexkeys = {modname .. ":tuxzite_adze"},
		nodes = {
			{match = adze, replace = "air"},
			{y = -1, match = modname .. ":tuxzite_rod", replace = "air"},
			{x = -1, match = adze, replace = "air"},
			{x = 1, match = adze, replace = "air"},
		},
		items = {{
				y = -1,
				name = modname .. ":tuxzite_rake"
		}}
	})

local function forge(from, fromqty, to, prills)
	return nodecore.register_craft({
			label = "forge tuxzite " .. (to or "prills"),
			action = "pummel",
			toolgroups = {thumpy = 3},
			indexkeys = {modname .. ":tuxzite_" .. from},
			nodes = {
				{
					match = {
						name = modname .. ":tuxzite_" .. from,
						count = fromqty
					},
					replace = "air"
				},
				{
					y = -1,
					match = modname .. ":silver"
				}
			},
			items = {
				to and (modname .. ":tuxzite_" .. to) or nil,
				prills and {
					name = modname .. ":tuxzite_prill",
					count = prills,
					scatter = 5
				} or nil
			}
		})
end
forge("prill", 3, "mallet_head")
forge("mallet_head", nil, "spade_head", 1)
forge("spade_head", nil, "hatchet_head")
forge("hatchet_head", nil, "pick_head", 1)
forge("pick_head", nil, nil, 1)

for a, b in pairs({spade = "pick", pick = "spade"}) do
	nodecore.register_craft({
			label = "weld tuxzite cattock",
			action = "pummel",
			toolgroups = {thumpy = 3},
			indexkeys = {modname .. ":tuxzite_" .. a .. "_head"},
			nodes = {
				{
					match = {
						name = modname .. ":tuxzite_" .. a .. "_head",
					},
					replace = "air"
				},
				{
					y = -1,
					match = {
						name = modname .. ":tuxzite_" .. b .. "_head",
					},
					replace = "air"
				},
				{
					y = -2,
					match = modname .. ":silver"
				}
			},
			items = {
				{
					name = modname .. ":tuxzite_cattock_head",
					y = -1
				}
			}
		})
end
