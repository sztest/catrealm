-- LUALOCALS < ---------------------------------------------------------
local minetest, nodecore
    = minetest, nodecore
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local silversounds = nodecore.sounds("nc_lode_tempered", nil, 2)

minetest.register_node(modname .. ":silver", {
		description = "Sterling",
		tiles = {"catrealm_ores_silver.png"},
		groups = {
			cracky = 3
		},
		sounds = silversounds,
		mapcolor = {r = 122, g = 177, b = 194},
	})

minetest.register_craftitem(modname .. ":silver_coin", {
		description = "Sterling Medallion",
		wield_image = "catrealm_ores_silver.png^[mask:catrealm_ores_silver_coin_mask.png",
		inventory_image = "catrealm_ores_silver.png^[mask:catrealm_ores_silver_coin_mask.png",
		sounds = silversounds,
	})

nodecore.register_craft({
		label = "extract silver from loose silver cobble",
		action = "pummel",
		indexkeys = {modname .. ":silver_cobble_loose"},
		nodes = {
			{match = modname .. ":silver_cobble_loose", replace = "nc_terrain:gravel"}
		},
		items = {
			{name = "nc_stonework:chip", count = 4, scatter = 5},
			{name = modname .. ":silver_coin", scatter = 5},
		},
		toolgroups = {cracky = 2},
		itemscatter = 5
	})
nodecore.register_craft({
		label = "extract silver from silver cobble",
		action = "pummel",
		indexkeys = {modname .. ":silver_cobble"},
		nodes = {
			{match = modname .. ":silver_cobble", replace = "nc_terrain:gravel"}
		},
		items = {
			{name = "nc_stonework:chip", count = 4, scatter = 5},
			{name = modname .. ":silver_coin", scatter = 5},
		},
		toolgroups = {cracky = 4},
		itemscatter = 5
	})

nodecore.register_craft({
		label = "pack silver cube",
		action = "pummel",
		toolgroups = {thumpy = 3},
		nodes = {
			{
				match = {name = modname .. ":silver_coin", count = 8},
				replace = modname .. ":silver"
			}
		},
	})
nodecore.register_craft({
		label = "unpack silver cube",
		action = "pummel",
		toolgroups = {choppy = 3},
		nodes = {
			{
				match = modname .. ":silver",
				replace = "air"
			}
		},
		items = {
			{name = modname .. ":silver_coin 2", count = 4, scatter = 5},
		},
	})
