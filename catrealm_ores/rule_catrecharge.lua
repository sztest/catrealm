-- LUALOCALS < ---------------------------------------------------------
local ipairs, minetest, nodecore, pairs, vector
    = ipairs, minetest, nodecore, pairs, vector
-- LUALOCALS > ---------------------------------------------------------

local catnodes = nodecore.group_expand({"group:nc_cats_cat"}, true)

local function rechargerate(pos)
	local amount = 0
	for _, p in ipairs(nodecore.find_nodes_around(pos,
			{"group:nc_cats_cat", "group:visinv"}, 5)) do
		local n = minetest.get_node(p).name
		if not catnodes[n] then
			n = nodecore.stack_get(p):get_name()
		end
		if catnodes[n] then
			local diff = vector.subtract(pos, p)
			local dsqr = vector.dot(diff, diff)
			if dsqr < 1 then dsqr = 1 end
			amount = amount + 1 / dsqr
		end
	end
	return amount
end

nodecore.register_playerstep({
		label = "cat recharging",
		action = function(player, _, dtime)
			if nodecore.stasis then return end
			local inv = player:get_inventory()
			local recharge
			local amount = 0
			for i = 1, inv:get_size("main") do
				local s = inv:get_stack("main", i)
				local n = not s:is_empty() and s:get_name()
				n = n and minetest.registered_items[n]
				local r = n and n.groups and n.groups.catrealm_cat_recharge
				if (r or 0) ~= 0 and s:get_wear() > 0 then
					recharge = recharge or {}
					recharge[i] = {stack = s, rate = r}
				end
				local c = n and n.groups and n.groups.nc_cats_cat
				if (c or 0) ~= 0 then
					amount = amount + 1
				end
			end
			if not recharge then return end
			local pos = player:get_pos()
			pos.y = pos.y + player:get_properties().eye_height
			amount = amount + rechargerate(pos)
			if amount <= 0 then return end
			for k, v in pairs(recharge) do
				local o = v.stack:get_wear()

				local w = o - amount * v.rate * dtime
				if w < 0 then w = 0 end
				if w == 0 and o ~= 0 then
					nodecore.player_discover(player, "cat recharging")
				end
				v.stack:set_wear(w)
				inv:set_stack("main", k, v.stack)
			end
		end
	})

nodecore.register_aism({
		label = "cat recharging",
		interval = 1,
		chance = 1,
		itemnames = {"group:catrealm_cat_recharge"},
		action = function(stack, data)
			if data.player then return end -- already handled by playerstep above
			if not data.pos then return end
			local amount = rechargerate(data.pos)
			if amount <= 0 then return end
			local def = minetest.registered_items[stack:get_name()]
			local o = stack:get_wear()
			local w = o - amount * def.groups.catrealm_cat_recharge
			if w < 0 then w = 0 end
			if w == 0 and o ~= 0 then
				nodecore.witness(data.pos, "cat recharging")
			end
			stack:set_wear(w)
			return stack
		end
	})
