-- LUALOCALS < ---------------------------------------------------------
local catrealm_core, minetest, nodecore
    = catrealm_core, minetest, nodecore
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local api = catrealm_core

minetest.register_node(modname .. ":tuxzite_cobble", {
		description = "Tuxzite Cobble",
		tiles = {"catrealm_ores_tuxzite.png^nc_terrain_cobble.png"},
		silktouch = {cracky = 6},
		sounds = nodecore.sounds("nc_terrain_stony"),
		groups = {
			rock = 1,
			cracky = 2,
			cobbley = 1,
			tuxzite_cobble = 1,
		},
		alternate_loose = {
			repack_level = 2,
			groups = {
				cracky = 0,
				crumbly = 2,
				falling_repose = 3
			},
			sounds = nodecore.sounds("nc_terrain_chompy")
		},
		mapcolor = {r = 76, g = 76, b = 76},
	})

minetest.register_node(modname .. ":tuxzite_ore", {
		description = "Tuxzite Ore",
		tiles = {"nc_terrain_stone.png^(catrealm_ores_tuxzite.png^[mask:catrealm_ores_ore_mask.png)"},
		groups = {
			cracky = 3
		},
		sounds = nodecore.sounds("nc_terrain_stony"),
		drop_in_place = modname .. ":tuxzite_cobble",
		silktouch = false,
		mapcolor = {r = 76, g = 76, b = 76},
	})

api.register_catrealm_ore({
		rarity = 1024,
		name = modname .. ":tuxzite_ore"
	})

local maxy = api.catrealm_maxy
local miny = api.catrealm_miny

local c_air = minetest.get_content_id("air")
local c_stone = minetest.get_content_id("nc_terrain:stone")
local c_tuxzite = minetest.get_content_id(modname .. ":tuxzite_ore")

nodecore.register_mapgen_shared({
		label = "tuxzite unexpose",
		func = function(minp, maxp, area, data)
			if minp.y > maxy or maxp.y < miny then return end
			local ai = area.index
			local ystride = area.ystride
			local zstride = area.zstride
			for z = minp.z, maxp.z do
				for y = minp.y, maxp.y do
					local offs = ai(area, 0, y, z)
					for x = minp.x, maxp.x do
						local i = offs + x
						if data[i] == c_tuxzite then
							if x == minp.x
							or x == maxp.x
							or y == minp.y
							or y == maxp.y
							or z == minp.z
							or z == maxp.z
							or data[i - 1] == c_air
							or data[i + 1] == c_air
							or data[i - ystride] == c_air
							or data[i + ystride] == c_air
							or data[i - zstride] == c_air
							or data[i + zstride] == c_air
							then data[i] = c_stone end
						end
					end
				end
			end
		end,
		priority = -15
	})
