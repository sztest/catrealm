-- LUALOCALS < ---------------------------------------------------------
local include, minetest, pairs, string
    = include, minetest, pairs, string
local string_gsub, string_sub
    = string.gsub, string.sub
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

include("ore_silver")
include("ore_tuxzite")

include("craft_basic_silver")
include("craft_basic_tuxzite")
include("craft_shafts")
include("craft_tools")

include("rule_catrecharge")
include("rule_purring")

include("renew_silver")
include("renew_tuxzite")

include("hints")

for k in pairs(minetest.registered_items) do
	if string_sub(k, 1, #modname + 1) == modname .. ":" then
		minetest.register_alias("catrealm" .. string_sub(k, #modname + 1), k)
	end
	local ks = string_gsub(k, "tuxzite", "cokezite")
	if ks ~= k then minetest.register_alias(ks, k) end
end
