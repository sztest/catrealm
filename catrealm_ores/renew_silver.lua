-- LUALOCALS < ---------------------------------------------------------
local minetest, nodecore
    = minetest, nodecore
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

nodecore.register_craft({
		label = "silver renewal",
		action = "cook",
		touchgroups = {flame = 3},
		neargroups = {coolant = 0},
		duration = 30,
		cookfx = true,
		indexkeys = {modname .. ":tuxzite_prill"},
		nodes = {
			{
				match = modname .. ":tuxzite_prill",
				replace = "air"
			},
			{
				y = -1,
				match = modname .. ":silver"
			}
		},
		items = {
			modname .. ":silver_coin"
		}
	})
