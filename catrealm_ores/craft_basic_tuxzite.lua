-- LUALOCALS < ---------------------------------------------------------
local minetest, nodecore
    = minetest, nodecore
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

minetest.register_node(modname .. ":tuxzite", {
		description = "Tuxzite",
		tiles = {"catrealm_ores_tuxzite.png"},
		groups = {
			cracky = 3
		},
		sounds = nodecore.sounds("nc_cats_mew", nil, 0.5),
		mapcolor = {r = 80, g = 80, b = 80},
	})

minetest.register_craftitem(modname .. ":tuxzite_prill", {
		description = "Tuxzite Prill",
		wield_image = "catrealm_ores_tuxzite.png^[mask:catrealm_ores_tuxzite_prill.png",
		inventory_image = "catrealm_ores_tuxzite.png^[mask:catrealm_ores_tuxzite_prill.png",
		sounds = nodecore.sounds("nc_cats_mew", nil, 0.75),
	})

nodecore.register_craft({
		label = "tuxzite extraction",
		action = "cook",
		touchgroups = {flame = 3},
		neargroups = {coolant = 0},
		duration = 30,
		cookfx = true,
		indexkeys = {"nc_cats:prill"},
		nodes = {
			{
				match = "nc_cats:prill",
				replace = "air"
			},
			{
				y = -1,
				match = {groups = {tuxzite_cobble = true}},
				replace = "nc_cats:egg"
			}
		},
		items = {
			modname .. ":tuxzite_prill"
		}
	})

nodecore.register_craft({
		label = "tuxzite injection",
		action = "pummel",
		toolgroups = {thumpy = 9},
		nodes = {
			{
				match = {name = modname .. ":tuxzite_prill"},
				replace = "air"
			},
			{
				y = -1,
				match = "nc_terrain:cobble_loose",
				replace = modname .. ":tuxzite_cobble"
			}
		},
	})

nodecore.register_craft({
		label = "pack tuxzite cube",
		action = "pummel",
		toolgroups = {thumpy = 3},
		nodes = {
			{
				match = {name = modname .. ":tuxzite_prill", count = 8},
				replace = modname .. ":tuxzite"
			},
			{
				y = -1,
				match = modname .. ":silver"
			}
		},
	})

nodecore.register_craft({
		label = "unpack tuxzite cube",
		action = "pummel",
		toolgroups = {choppy = 3},
		nodes = {
			{
				match = modname .. ":tuxzite",
				replace = "air"
			},
			{
				y = -1,
				match = modname .. ":silver"
			}
		},
		items = {
			{name = modname .. ":tuxzite_prill 2", count = 4, scatter = 5},
		},
	})
