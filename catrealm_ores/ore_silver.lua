-- LUALOCALS < ---------------------------------------------------------
local catrealm_core, minetest, nodecore
    = catrealm_core, minetest, nodecore
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local api = catrealm_core

minetest.register_node(modname .. ":silver_cobble", {
		description = "Sterling Cobble",
		tiles = {"catrealm_ores_silver.png^nc_terrain_cobble.png"},
		silktouch = {cracky = 6},
		sounds = nodecore.sounds("nc_terrain_stony"),
		groups = {
			rock = 1,
			cracky = 2,
			cobbley = 1
		},
		alternate_loose = {
			repack_level = 2,
			groups = {
				cracky = 0,
				crumbly = 2,
				falling_repose = 3
			},
			sounds = nodecore.sounds("nc_terrain_chompy")
		},
		mapcolor = {r = 101, g = 138, b = 149},
	})

minetest.register_node(modname .. ":silver_ore", {
		description = "Sterling Ore",
		tiles = {"nc_terrain_stone.png^(catrealm_ores_silver.png^[mask:catrealm_ores_ore_mask.png)"},
		groups = {
			cracky = 3
		},
		sounds = nodecore.sounds("nc_terrain_stony"),
		drop_in_place = modname .. ":silver_cobble",
		silktouch = false,
		mapcolor = {r = 101, g = 138, b = 149},
	})

api.register_catrealm_ore({
		rarity = 256,
		name = modname .. ":silver_ore"
	})
