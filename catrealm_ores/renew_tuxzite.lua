-- LUALOCALS < ---------------------------------------------------------
local math, minetest, nodecore, string
    = math, minetest, nodecore, string
local math_random, string_format
    = math.random, string.format
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local timecost = 60

nodecore.register_soaking_abm({
		label = "tuxzite renewal",
		fieldname = "tuxziterenew",
		nodenames = {modname .. ":tuxzite"},
		interval = 5,
		chance = 1,
		arealoaded = 1,
		soakrate = function(pos)
			local above = {x = pos.x, y = pos.y + 1, z = pos.z}
			local anode = minetest.get_node(above)
			if minetest.get_item_group(anode.name, "is_stack_only") < 1 then return false end
			local stack = nodecore.stack_get(above)
			if stack:get_name() ~= "nc_cats:prill" then return false end
			return 1
		end,
		soakcheck = function(data, pos)
			for _ = 1, 10 do
				if data.total < timecost then return data.total end
				data.total = data.total - timecost
				local above = {x = pos.x, y = pos.y + 1, z = pos.z}
				nodecore.stack_sounds(above, "dug")
				local stack = nodecore.stack_get(above)
				local conv = stack:take_item(1)
				nodecore.stack_set(above, stack)
				if math_random(1, 24) == 1 then
					nodecore.witness(above, "tuxzite renewal")
					conv:set_name(modname .. ":tuxzite_prill")
				end
				above.y = above.y + 1
				nodecore.log("action", string_format("tuxzite renewal ejects %s at %s",
						conv:get_name(), minetest.pos_to_string(above)))
				nodecore.item_eject(above, conv, 5, 1, {x = 0, y = 5, z = 0})
			end
			return data.total
		end
	})
