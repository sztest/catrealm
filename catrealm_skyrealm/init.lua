-- LUALOCALS < ---------------------------------------------------------
local nodecore
    = nodecore
-- LUALOCALS > ---------------------------------------------------------

nodecore.register_craft({
		label = "chisel catrealm skyrealm portal",
		action = "pummel",
		toolgroups = {thumpy = 3},
		normal = {y = 1},
		indexkeys = {"group:chisel"},
		nodes = {
			{
				match = {groups = {chisel = 2}},
				dig = true
			},
			{
				y = -1,
				match = "catrealm_ores:tuxzite",
				replace = "nc_skyrealm:portal"
			}
		},
		items = {
			{name = "catrealm_ores:tuxzite_prill", count = 4, scatter = 5},
		},
	})

nodecore.register_hint("chisel a skyrealm portal out of tuxzite",
	"chisel catrealm skyrealm portal",
	{"catrealm_ores:tuxzite", "catrealm_ores:tuxzite_prill"}
)
