-- LUALOCALS < ---------------------------------------------------------
local minetest, nodecore
    = minetest, nodecore
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

nodecore.register_hint("smother a lode cube with cats to make a portal",
	"cook catrealm portal",
	{"group:nc_cats_cat", "nc_lode:block_annealed"}
)
nodecore.register_hint("use a cat prill to enter the catrealm",
	"catrealm teleport",
	"cook catrealm portal"
)

nodecore.register_hint("find a cat shrub bulb",
	modname .. ":bulb",
	"catrealm teleport"
)
nodecore.register_hint("plant a cat shrub bulb",
	"cat bulb planting",
	modname .. ":bulb"
)

nodecore.register_hint("fall back to surface from catrealm",
	"catrealm return",
	"catrealm teleport"
)
