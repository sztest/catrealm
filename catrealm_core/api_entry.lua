-- LUALOCALS < ---------------------------------------------------------
local ipairs, math, minetest, nodecore, vector
    = ipairs, math, minetest, nodecore, vector
local math_random
    = math.random
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local api = _G[modname]
local modstore = minetest.get_mod_storage()

local maxy = api.catrealm_maxy
local miny = api.catrealm_miny
local centery = (miny + maxy) / 2

local db
do
	local s = modstore:get_string("entrypoints")
	db = s and s ~= "" and minetest.deserialize(s) or {}
end
local function savedb()
	modstore:set_string("entrypoints", minetest.serialize(db))
end

local function checkentry(pos)
	local air = 0
	for y = pos.y, pos.y + 16 do
		pos.y = y
		local node = minetest.get_node(pos)
		if node.name == "ignore" then
			return
		elseif node.name == "air" then
			air = air + 1
			if air >= 2 then
				pos.y = pos.y - 1
				db[#db + 1] = pos
				nodecore.log("action", "found catrealm entry at " .. minetest.pos_to_string(pos))
				return savedb()
			end
		else
			air = 0
		end
	end
end

local shuttingdown
minetest.register_on_shutdown(function() shuttingdown = true end)

local chunkoffs = {x = 32, y = 32, z = 32}
local chunkmax = {x = 79, y = 79, z = 79}
local blockmax = {x = 15, y = 15, z = 15}
local function preload()
	if shuttingdown or (#db > 0) then return end
	local chunkpos = {
		x = math_random(-30000, 30000),
		y = centery,
		z = math_random(-30000, 30000)
	}
	chunkpos = vector.multiply(vector.round(vector.multiply(vector.subtract(
					chunkpos, chunkoffs), 1/80)), 80)
	local chunkend = vector.add(chunkpos, chunkmax)
	nodecore.log("action", "catrealm entry searching from " .. minetest.pos_to_string(chunkpos)
		.. " to " .. minetest.pos_to_string(chunkend))
	minetest.emerge_area(chunkpos, chunkend, function(blockpos, _, rem)
			local minp = vector.multiply(blockpos, 16)
			local maxp = vector.add(minp, blockmax)
			for _, p in ipairs(minetest.find_nodes_in_area(minp, maxp, {modname .. ":stem"})) do
				checkentry(p)
			end
			if rem > 0 then return end
			if #db > 0 then
				return nodecore.log("action", "catrealm entry searching finished")
			else
				return preload()
			end
		end)
end
minetest.after(0, preload)

function api.get_entrypoint()
	local found = db[math_random(1, #db)]
	db = {}
	savedb()
	preload()
	return found
end
