-- LUALOCALS < ---------------------------------------------------------
local PerlinNoise, ipairs, math, minetest, nodecore, vector
    = PerlinNoise, ipairs, math, minetest, nodecore, vector
local math_abs
    = math.abs
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local api = _G[modname]

-- catrealm ore definition:
-- - rarity: only appears in 1 out of N nodes
-- - name: name of node to replace stone with
api.register_catrealm_ore,
api.registered_catrealm_ores
= nodecore.mkreg()

local maxy = api.catrealm_maxy
local miny = api.catrealm_miny
local centery = (miny + maxy) / 2

local maxydist = 20
maxy = centery + maxydist
miny = centery - maxydist

local noise = PerlinNoise{
	scale = 1,
	spread = {x = 128, y = 64, z = 128},
	seed = 12974,
	octaves = 5,
	persistence = 0.95,
	lacunarity = 2,
	flags = "eased"
}

local c_air = minetest.get_content_id("air")
local c_dirt = minetest.get_content_id("nc_terrain:dirt")
local c_stone = minetest.get_content_id("nc_terrain:stone")
local c_grass = minetest.get_content_id(modname .. ":grass")
local c_stem = minetest.get_content_id(modname .. ":stem")
local c_leaves = minetest.get_content_id(modname .. ":leaves")
local c_cat = minetest.get_content_id(modname .. ":catgen")

local layers = {c_grass, c_dirt, c_dirt}
for i = 4, 100 do layers[i] = c_stone end

local function shrubify(area, data, center, pos, minp, maxp, rng)
	local diff = vector.subtract(center, pos)
	local dsqr = vector.dot(diff, diff)
	if dsqr > rng(1, 3) then return end

	if pos.x > maxp.x or pos.x < minp.x or pos.y > maxp.y or pos.y < minp.y
	or pos.z > maxp.z or pos.z < minp.z then return end

	local i = area:index(pos.x, pos.y, pos.z)
	if data[i] ~= c_air then return end

	data[i] = c_leaves
	pos.x = pos.x + 1
	shrubify(area, data, center, pos, minp, maxp, rng)
	pos.x = pos.x - 2
	shrubify(area, data, center, pos, minp, maxp, rng)
	pos.x = pos.x + 1
	pos.z = pos.z + 1
	shrubify(area, data, center, pos, minp, maxp, rng)
	pos.z = pos.z - 2
	shrubify(area, data, center, pos, minp, maxp, rng)
	pos.z = pos.z + 1
	pos.y = pos.y + 1
	return shrubify(area, data, center, pos, minp, maxp, rng)
end

local function init_ores()
	init_ores = function() end
	for _, def in ipairs(api.registered_catrealm_ores) do
		def.id = minetest.get_content_id(def.name)
	end
end

nodecore.register_mapgen_shared({
		label = "catrealm",
		func = function(minp, maxp, area, data, _, _, _, rng)
			if minp.y > maxy or maxp.y < miny then return end
			init_ores()
			local ai = area.index
			local ystride = area.ystride
			local pos = {}
			local myminy = miny > minp.y and miny or minp.y
			local mymaxy = maxy < maxp.y and maxy or maxp.y
			for z = minp.z, maxp.z do
				pos.z = z
				for x = minp.x, maxp.x do
					pos.x = x
					local offs = ai(area, x, 0, z)
					local depth = 1
					local exdirt
					for y = mymaxy, myminy, -1 do
						pos.y = y
						local dy = math_abs(y - centery)
						local r = noise:get_3d(pos) * 10 - 10
						if r > dy then
							exdirt = nil
							local i = offs + y * ystride
							if depth == 1 and rng(1, 256) == 1 then
								data[i] = c_stem
								local shrubpos = {x = pos.x, y = pos.y + 1, z = pos.z}
								local cpos = {x = pos.x, y = pos.y + 1, z = pos.z}
								shrubify(area, data, cpos, shrubpos, minp, maxp, rng)
								exdirt = true
							else
								data[i] = layers[depth]
								if data[i] == c_stone then
									local ores = api.registered_catrealm_ores
									for j = 1, #ores do
										if rng(1, ores[j].rarity) == 1 then
											data[i] = ores[j].id
											break
										end
									end
								end
								if y < maxp.y and rng(1, 160) == 1 then
									local j = offs + (y + 1) * ystride
									if data[j] == c_air then
										data[j] = c_cat
									end
								end
							end
							depth = depth + 1
						else
							if exdirt then
								local i = offs + y * ystride
								data[i] = c_dirt
							elseif depth > 1 then
								depth = 2
							end
							exdirt = nil
						end
					end
				end
			end
		end,
		priority = -10
	})
