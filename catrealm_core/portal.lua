-- LUALOCALS < ---------------------------------------------------------
local math, minetest, nc_cats, nodecore, string
    = math, minetest, nc_cats, nodecore, string
local math_cos, math_pi, math_random, math_sin, string_format
    = math.cos, math.pi, math.random, math.sin, string.format
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local api = _G[modname]

local metakey = "catrealm"

local function touchtip(name, meta)
	local pos = meta:get_string(metakey)
	if pos == "" then return name end
	pos = minetest.string_to_pos(pos)
	return name .. "\n" .. nodecore.notranslate(string_format("%x", minetest.hash_node_position(pos)):upper())
end

local txr = "catrealm_core_portal_base.png"
minetest.register_node(modname .. ":portal", {
		description = "CatRealm Portal",
		tiles = {
			txr .. "^(nc_cats_ears.png^[invert:rgb)",
			txr .. "^(nc_cats_paws_front.png^nc_cats_paws_back.png^[invert:rgb)",
			txr,
			txr,
			txr .. "^(nc_cats_tail.png^[invert:rgb)",
			txr .. "^(nc_cats_face.png^[invert:rgb)",
		},
		paramtype2 = "facedir",
		groups = {
			snappy = 1,
			stack_as_node = 1,
			falling_node = 1,
		},
		node_placement_prediction = "nc_items:stack",
		stack_max = 1,
		sounds = nodecore.sounds("nc_cats_mew"),
		on_place = function(stack, placer, pointed, inf, orient, ...)
			orient = orient or {}
			orient.invert_wall = pointed.above.y == pointed.under.y
			return minetest.rotate_and_place(stack, placer, pointed, inf, orient, ...)
		end,
		preserve_metadata = function(_, _, oldmeta, drops)
			drops[1]:get_meta():set_string(metakey, oldmeta[metakey])
		end,
		after_place_node = function(pos, _, itemstack)
			nc_cats.checkfacedir(pos)
			minetest.get_meta(pos):set_string(metakey,
				itemstack:get_meta():get_string(metakey))
		end,
		on_stack_touchtip = function(stack, name)
			return touchtip(name, stack:get_meta())
		end,
		on_node_touchtip = function(pos, _, name)
			return touchtip(name, minetest.get_meta(pos))
		end,
		mapcolor = {r = 0, g = 0, b = 0},
	})

local function assign_meta(meta, npos)
	local old = meta:get_string(metakey)
	if old ~= "" then return end

	local pos = api.get_entrypoint()
	if not pos then return end

	meta:set_string(metakey, minetest.pos_to_string(pos))
	nodecore.log("action", "assigned catrealm entry " .. minetest.pos_to_string(pos)
		.. " to portal" .. (npos and (" at " .. minetest.pos_to_string(npos))))
	return true
end

nodecore.register_aism({
		label = "catrealm assign",
		interval = 1,
		chance = 1,
		itemnames = {modname .. ":portal"},
		action = function(stack, data)
			return assign_meta(stack:get_meta(), data.pos) and stack
		end
	})
minetest.register_abm({
		label = "catrealm assign",
		interval = 1,
		chance = 1,
		nodenames = {modname .. ":portal"},
		action = function(pos)
			return assign_meta(minetest.get_meta(pos), pos)
		end
	})

local function megapurr(pos, pname)
	local purrid = math_random(1, 4)
	for t = 1, 5 do
		local pp = {x = pos.x, y = pos.y, z = pos.z}
		pp.x = pp.x + 5 * math_sin(t * 2/5 * math_pi)
		pp.z = pp.z + 5 * math_cos(t * 2/5 * math_pi)
		nodecore.sound_play("nc_cats_purr_" .. purrid, {
				pos = pp,
				gain = 4,
				to_player = pname
			})
	end
end

nodecore.register_craft({
		label = "catrealm teleport",
		action = "pummel",
		duration = 2,
		wield = {name = "nc_cats:prill"},
		consumewield = 1,
		check = function(pos)
			return (not api.in_catrealm(pos))
			and minetest.get_meta(pos):get_string(metakey) ~= ""
		end,
		nodes = {
			{
				match = modname .. ":portal"
			}
		},
		after = function(pos, data)
			if not data.crafter then return end
			local meta = minetest.get_meta(pos)
			local tpos = meta:get_string(metakey)
			if tpos == "" then return end
			api.player_enter(data.crafter, minetest.string_to_pos(tpos), pos)
			local pp = data.crafter:get_pos()
			pp.y = pp.y + data.crafter:get_properties().eye_height
			megapurr(pp, data.crafter:get_player_name())
		end
	})

local function delaymeow(pos)
	return minetest.after(math_random() / 2,
		function() nodecore.node_sound(pos, "place") end)
end
nodecore.register_craft({
		label = "cook catrealm portal",
		action = "cook",
		touchgroups = {nc_cats_face = 6},
		duration = 30,
		nodes = {{match = "nc_lode:block_annealed", replace = modname .. ":portal"}},
		inprogress = function(pos)
			local found = nodecore.find_nodes_around(pos, "group:nc_cats_face")
			for i = #found, 2, -1 do
				local j = math_random(1, i)
				found[i], found[j] = found[j], found[i]
			end
			delaymeow(found[1])
			delaymeow(found[2])
		end,
		after = function(pos)
			assign_meta(minetest.get_meta(pos), pos)
			megapurr(pos)
		end
	})
nodecore.register_cook_abm({
		nodenames = {"nc_lode:block_annealed"},
		neighbors = {"group:nc_cats_cat"}
	})
