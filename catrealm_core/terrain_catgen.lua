-- LUALOCALS < ---------------------------------------------------------
local minetest, nc_cats
    = minetest, nc_cats
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

minetest.register_node(modname .. ":catgen", {
		description = "Delayed Cat Spawn",
		drawtype = "airlike",
		sunlight_propagates = true,
		paramtype = "light",
		pointable = false,
		buildable_to = true,
		walkable = true,
	})

minetest.register_lbm({
		name = modname .. ":catgen",
		run_at_every_load = true,
		nodenames = {modname .. ":catgen"},
		action = function(pos)
			return nc_cats.makecat(pos)
		end
	})
minetest.register_abm({
		name = modname .. ":catgen",
		nodenames = {modname .. ":catgen"},
		interval = 1,
		chance = 1,
		action = function(pos)
			return nc_cats.makecat(pos)
		end
	})
