-- LUALOCALS < ---------------------------------------------------------
local include, minetest, pairs, rawget, rawset, string
    = include, minetest, pairs, rawget, rawset, string
local string_sub
    = string.sub
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local api = rawget(_G, modname) or {}
rawset(_G, modname, api)

api.catrealm_maxy = 20000
api.catrealm_miny = 15000

function api.in_catrealm(pos)
	return pos.y >= api.catrealm_miny - 0.5
	and pos.y <= api.catrealm_maxy + 0.5
end

include("api_entry")
include("api_player")

include("terrain_grass")
include("terrain_shrub")
include("terrain_catgen")

include("mapgen")
include("portal")

include("rule_playerfall")
include("rule_itemfall")

include("skybox")

include("hints")

for k in pairs(minetest.registered_items) do
	if string_sub(k, 1, #modname + 1) == modname .. ":" then
		minetest.register_alias("catrealm" .. string_sub(k, #modname + 1), k)
	end
end
