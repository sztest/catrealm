-- LUALOCALS < ---------------------------------------------------------
local ItemStack, ipairs, math, minetest, next, nodecore, pairs, string,
      vector
    = ItemStack, ipairs, math, minetest, next, nodecore, pairs, string,
      vector
local math_random, string_format
    = math.random, string.format
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local api = _G[modname]
local modstore = minetest.get_mod_storage()

local storekey = "falling"
local s = modstore:get_string(storekey)
local cache = s and s ~= "" and minetest.deserialize(s) or {}

local function getfallingthings(rpos)
	local key = minetest.pos_to_string(rpos)
	local found = cache[key] or {item = {}, node = {}}
	return found, function()
		cache[key] = found
		return modstore:set_string(storekey, minetest.serialize(cache))
	end
end

local function fallingfor(self)
	local pos = self.object:get_pos()
	if not (pos and api.in_catrealm(pos)) then return end
	local vel = self.object:get_velocity()
	if (not vel) or vel.y > -30 then return end

	local oy = pos.y
	while true do
		local node = minetest.get_node(pos)
		if node.name == "ignore" then break end
		if node.name ~= "air" then return end
		pos.y = pos.y - 1
	end
	pos.y = oy

	local player
	local pdsqr
	for _, p in ipairs(minetest.get_connected_players()) do
		local diff = vector.subtract(pos, p:get_pos())
		local dsqr = vector.dot(diff, diff)
		if not player then
			player = p
			pdsqr = dsqr
		elseif dsqr < pdsqr then
			pdsqr = dsqr
			player = p
		end
	end
	if not player then return end

	local data = api.playerdata(player)
	return data.portal or data.rtn
end

local function itemshortdesc(stack)
	stack = ItemStack(stack)
	local copy = ItemStack(stack:get_name())
	copy:set_count(stack:get_count())
	return copy:to_string()
end

nodecore.register_item_entity_step(function(self)
		local rpos = fallingfor(self)
		if not rpos then return end

		local pt, save = getfallingthings(rpos)
		pt = pt.item
		local stack = ItemStack(self.itemstring)
		for i = 1, #pt do
			local pts = ItemStack(pt[i])
			stack = pts:add_item(stack)
			pt[i] = pts:to_string()
			if stack:is_empty() then break end
		end
		if not stack:is_empty() then
			pt[#pt + 1] = stack:to_string()
		end
		save()

		nodecore.log("action", string_format("catrealm dropped item %q at %s for %s",
				itemshortdesc(stack), minetest.pos_to_string(self.object:get_pos(), 0),
				minetest.pos_to_string(rpos)))
		return self.object:remove()
	end)

nodecore.register_falling_node_step(function(self)
		local rpos = fallingfor(self)
		if not rpos then return end

		local pt, save = getfallingthings(rpos)
		pt = pt.node
		pt[#pt + 1] = {
			n = self.node,
			m = self.meta
		}
		save()

		nodecore.log("action", string_format("catrealm dropped node %q at %s for %s",
				self.node.name, minetest.pos_to_string(self.object:get_pos(), 0),
				minetest.pos_to_string(rpos)))
		return self.object:remove()
	end)

local function findspot_start(pos)
	if nodecore.near_unloaded(pos, nil, 5) then return end
	for rel in nodecore.settlescan() do
		local p = vector.add(pos, rel)
		if nodecore.buildable_to(p) then return p end
	end
end
local return_falling_entitites = {
	["__builtin:falling_node"] = true,
	["__builtin:item"] = true,
}
local function not_too_many_objects(pos)
	local count = 0
	for _, obj in ipairs(minetest.get_objects_inside_radius(pos, 16)) do
		if return_falling_entitites[obj.name] then
			count = count + 1
		end
	end
	return count < 5
end
local function findspot(pos)
	pos = findspot_start(pos)
	if not pos then return end
	local tpos = vector.add(pos, {
			x = math_random() * 10 - 5,
			y = math_random() * 10 + 5,
			z = math_random() * 10 - 5,
		})
	for hit in minetest.raycast(pos, tpos, false, false) do
		if hit.above and not vector.equals(pos, hit.above)
		and hit.under and not vector.equals(pos, hit.under)
		and nodecore.buildable_to(hit.above)
		and not_too_many_objects(hit.above)
		then return hit.above end
	end
	return tpos
end

local function return_falling(pos, data)
	local dirty

	local pt = data.item
	for i = #pt, 1, -1 do
		local p = findspot(pos)
		if p then
			nodecore.item_eject(p, pt[i], 1)
			nodecore.log("action", string_format(
					"catrealm item %q arrived at %s for %s",
					itemshortdesc(pt[i]), minetest.pos_to_string(p, 0),
					minetest.pos_to_string(pos)))
			pt[i] = nil
			dirty = true
		else
			break
		end
	end

	pt = data.node
	for i = #pt, 1, -1 do
		local p = findspot(pos)
		if p then
			local obj = minetest.add_entity(p, "__builtin:falling_node")
			if obj then
				obj:get_luaentity():set_node(pt[i].n, pt[i].m)
				nodecore.log("action", string_format(
						"catrealm node %q arrived at %s for %s",
						pt[i], minetest.pos_to_string(p, 0),
						minetest.pos_to_string(pos)))
				pt[i] = nil
				dirty = true
			else
				break
			end
		else
			break
		end
	end

	return dirty
end
nodecore.interval(2, function()
		local dirty
		local clear = {}
		for k, v in pairs(cache) do
			local pos = minetest.string_to_pos(k)
			if pos then
				if minetest.get_node_or_nil(pos) then
					dirty = return_falling(pos, v) or dirty
				end
			else
				-- legacy database by player name
				local player = minetest.get_player_by_name(k)
				if player then
					local ppos = player:get_pos()
					if not api.in_catrealm(ppos) then
						dirty = return_falling(ppos, v) or dirty
					end
				end
			end
			if #v.item == 0 and #v.node == 0 then
				clear[k] = true
			end
		end
		if dirty or next(clear) then
			for k in pairs(clear) do cache[k] = nil end
			return modstore:set_string(storekey, minetest.serialize(cache))
		end
	end)
