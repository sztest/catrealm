-- LUALOCALS < ---------------------------------------------------------
local minetest, nodecore, pairs, vector
    = minetest, nodecore, pairs, vector
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local api = _G[modname]

local maxy = (api.catrealm_miny + api.catrealm_maxy) / 2 - 16

local function airscan(pos)
	local nn = minetest.get_node(pos).name
	if nn == "ignore" then return true end
	if nn ~= "air" then return end
	pos.y = pos.y - 1
	return airscan(pos)
end
local function scanaround(pos)
	return airscan({x = pos.x, y = pos.y, z = pos.z})
	and airscan({x = pos.x + 1, y = pos.y, z = pos.z})
	and airscan({x = pos.x - 1, y = pos.y, z = pos.z})
	and airscan({x = pos.x, y = pos.y, z = pos.z + 1})
	and airscan({x = pos.x, y = pos.y, z = pos.z - 1})
	and airscan({x = pos.x + 1, y = pos.y, z = pos.z + 1})
	and airscan({x = pos.x + 1, y = pos.y, z = pos.z - 1})
	and airscan({x = pos.x - 1, y = pos.y, z = pos.z + 1})
	and airscan({x = pos.x - 1, y = pos.y, z = pos.z - 1})
end
local function checkplayer(player, dtime)
	local pos = player:get_pos()
	if pos.y > maxy or not api.in_catrealm(pos) then return end
	local data, save = api.playerdata(player)
	if player:get_velocity().y > 0 or not scanaround(pos) then
		local rpos = vector.round(pos)
		if data.falling or not (data.laststand
			and vector.equals(data.laststand, rpos)) then
			data.laststand = rpos
			data.falling = nil
			return save()
		end
		return
	end
	data.falling = (data.falling or 0) + dtime
	if data.falling >= 5 then
		data.falling = nil
		nodecore.player_discover(player, "catrealm return")
		return api.player_return(player)
	end
	return save()
end
minetest.register_globalstep(function(dtime)
		for _, player in pairs(minetest.get_connected_players()) do
			checkplayer(player, dtime)
		end
	end)
