-- LUALOCALS < ---------------------------------------------------------
local math, minetest, nodecore, vector
    = math, minetest, nodecore, vector
local math_random
    = math.random
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local function fade(txr)
	return txr .. "^[multiply:#a0a0a0^" .. txr
end

minetest.register_node(modname .. ":leaves", {
		description = "Cat Leaves",
		drawtype = "allfaces_optional",
		paramtype = "light",
		tiles = {fade("catrealm_core_leaves.png")},
		waving = 1,
		air_pass = true,
		silktouch = false,
		no_repack = true,
		groups = {
			canopy = 1,
			snappy = 1,
			flammable = 3,
			fire_fuel = 2,
			scaling_time = 90,
		},
		node_dig_prediction = "air",
		use_texture_alpha = "clip",
		sounds = nodecore.sounds("nc_terrain_swishy"),
		alternate_loose = {
			tiles = {fade("catrealm_core_leaves_dry.png")},
			walkable = false,
			groups = {
				canopy = 0,
				leafy = 1,
				flammable = 1,
				falling_repose = 1,
				stack_as_node = 1,
				peat_grindable_item = 1
			},
			mapcolor = {r = 41, g = 21, b = 49},
		},
		alternate_solid = {
			node_dig_prediction = "air",
			after_dig_node = function(pos)
				local rnd = math_random(1, 8)
				if rnd == 1 then
					nodecore.set_loud(pos, {name = "nc_tree:stick"})
				elseif rnd <= 4 then
					minetest.set_node(pos, {name = modname .. ":leaves_loose"})
				else
					minetest.remove_node(pos)
				end
			end,
			mapcolor = {r = 54, g = 8, b = 72},
		},
	})

minetest.register_craftitem(modname .. ":bulb", {
		description = "Cat Shrub Bulb",
		wield_image = "catrealm_core_shrub_bulb.png",
		inventory_image = "catrealm_core_shrub_bulb.png",
		sounds = nodecore.sounds("nc_tree_corny")
	})

local diag = 2 ^ 0.5 / 32
minetest.register_node(modname .. ":stem", {
		description = "Cat Shrub Stem",
		selection_box = nodecore.fixedbox(
			{-0.5, -0.5, -0.5, 0.5, 0.5, 0.5},
			{-5 * diag, 0.5, -5 * diag, 5 * diag, 18/16, 5 * diag}
		),
		falling_visual = "nc_terrain:dirt",
		drawtype = "plantlike_rooted",
		tiles = {"nc_terrain_dirt.png"},
		special_tiles = {"catrealm_core_shrub_stem.png"},
		paramtype = "light",
		groups = {
			soil = 1,
			crumbly = 1
		},
		drop = "nc_woodwork:plank",
		after_dig_node = function(pos)
			pos.y = pos.y + 1
			if math_random(1, 4) == 1 then nodecore.item_eject(pos, modname .. ":bulb", 2) end
			return nodecore.item_eject(pos, modname .. ":bulb", 0, 1, {x = 0, y = 2, z = 0})
		end,
		drop_in_place = "nc_terrain:dirt_loose",
		silktouch = false,
		sounds = nodecore.sounds("nc_terrain_crunchy"),
		mapcolor = minetest.registered_nodes["nc_terrain:dirt"].mapcolor,
	})

minetest.register_node(modname .. ":sprout", {
		description = "Cat Shrub Sprout",
		selection_box = nodecore.fixedbox(
			{-0.5, -0.5, -0.5, 0.5, 0.5, 0.5},
			{-diag, 0.5, -diag, diag, 10/16, diag}
		),
		falling_visual = "nc_terrain:dirt_loose",
		drawtype = "plantlike_rooted",
		tiles = {"nc_terrain_dirt.png^nc_api_loose.png"},
		special_tiles = {"catrealm_core_shrub_sprout.png"},
		paramtype = "light",
		groups = {
			soil = 1,
			crumbly = 1
		},
		drop = "nc_terrain:dirt_loose",
		silktouch = false,
		sounds = nodecore.sounds("nc_terrain_crunchy"),
		mapcolor = minetest.registered_nodes["nc_terrain:dirt"].mapcolor,
	})

local function soilboost(pos, name)
	local def = minetest.registered_items[name]
	local soil = def.groups.soil or 0
	if soil > 2 then
		nodecore.soaking_abm_push(pos, "catshrub", (soil - 2) * 300)
		nodecore.soaking_particles(pos, (soil - 2) * 10,
			0.5, .45, modname .. ":leaves")
	end
end
nodecore.register_craft({
		label = "cat bulb planting",
		action = "stackapply",
		wield = {groups = {dirt_loose = true}},
		consumewield = 1,
		indexkeys = {modname .. ":bulb"},
		nodes = {{match = modname .. ":bulb", replace = modname .. ":sprout"}},
		after = function(pos, data)
			soilboost(pos, data.wield:get_name())
		end
	})

local sproutcost = 1000
local function growparticles(pos, rate, width)
	nodecore.soaking_particles(pos, rate, 10, width, modname .. ":leaves")
end
local function shrubify(center, pos)
	local diff = vector.subtract(center, pos)
	local dsqr = vector.dot(diff, diff)
	if dsqr > math_random(1, 3) then return end

	if minetest.get_node(pos).name ~= "air" then return end
	nodecore.set_loud(pos, {name = modname .. ":leaves"})

	pos.x = pos.x + 1
	shrubify(center, pos)
	pos.x = pos.x - 2
	shrubify(center, pos)
	pos.x = pos.x + 1
	pos.z = pos.z + 1
	shrubify(center, pos)
	pos.z = pos.z - 2
	shrubify(center, pos)
	pos.z = pos.z + 1
	pos.y = pos.y + 1
	return shrubify(center, pos)
end
nodecore.register_soaking_abm({
		label = "cat shrub sprout",
		fieldname = "catshrub",
		nodenames = {modname .. ":sprout"},
		interval = 10,
		arealoaded = 1,
		soakrate = nodecore.tree_growth_rate,
		soakcheck = function(data, pos)
			if data.total >= sproutcost then
				nodecore.set_loud(pos, {name = modname .. ":stem"})
				local cpos = {x = pos.x, y = pos.y + 1, z = pos.z}
				local apos = {x = pos.x, y = pos.y + 1, z = pos.z}
				shrubify(cpos, apos)
			end
			return growparticles(pos, data.rate, 0.2)
		end
	})

nodecore.register_craft({
		label = "tickle cat shrub",
		action = "pummel",
		toolgroups = {cuddly = 1},
		normal = {y = 1},
		nodes = {
			{match = {name = modname .. ":sprout", stacked = false}}
		},
		after = function(pos)
			nodecore.soaking_abm_tickle(pos, "catshrub")
			nodecore.soaking_particles(pos, 25, 0.5, .45, modname .. ":leaves")
		end
	})
