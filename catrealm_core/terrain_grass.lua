-- LUALOCALS < ---------------------------------------------------------
local minetest, nodecore
    = minetest, nodecore
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local api = _G[modname]

local miny = api.catrealm_miny

local catgrass = modname .. ":grass"
local reggrass = "nc_terrain:dirt_with_grass"
local dirt = "nc_terrain:dirt"

minetest.register_node(catgrass, {
		description = "Cat Grass",
		tiles = {
			"catrealm_core_grass_top.png",
			"nc_terrain_dirt.png",
			"nc_terrain_dirt.png^(catrealm_core_grass_top.png^[mask:nc_terrain_grass_sidemask.png)"
		},
		groups = {
			soil = 1,
			crumbly = 2
		},
		drop_in_place = "nc_terrain:dirt",
		sounds = nodecore.sounds("nc_terrain_grassy"),
		mapcolor = {r = 173, g = 20, b = 0},
	})

minetest.register_abm({
		label = "cat grass spread",
		nodenames = {"group:grassable"},
		neighbors = {catgrass},
		neighbors_invert = true,
		interval = 2,
		chance = 50,
		action = function(pos)
			local above = {x = pos.x, y = pos.y + 1, z = pos.z}
			if not nodecore.can_grass_grow_under(above) then return end
			return minetest.set_node(pos, {name = catgrass})
		end
	})

minetest.register_abm({
		label = "cat grass decay",
		nodenames = {catgrass},
		interval = 4,
		chance = 50,
		action = function(pos)
			if pos.y >= miny then
				local above = {x = pos.x, y = pos.y + 1, z = pos.z}
				if nodecore.can_grass_grow_under(above) ~= false then return end
			end
			return minetest.set_node(pos, {name = dirt})
		end
	})

minetest.register_abm({
		label = "cat grass spread onto regular grass",
		nodenames = {reggrass},
		neighbors = {catgrass},
		neighbors_invert = true,
		interval = 2,
		chance = 20,
		action = function(pos)
			if pos.y < miny then return end
			local above = {x = pos.x, y = pos.y + 1, z = pos.z}
			if not nodecore.can_grass_grow_under(above) then return end
			return minetest.set_node(pos, {name = catgrass})
		end
	})
