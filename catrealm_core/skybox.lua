-- LUALOCALS < ---------------------------------------------------------
local PcgRandom, math, minetest, nodecore, string, tostring
    = PcgRandom, math, minetest, nodecore, string, tostring
local math_ceil, math_floor, math_pi, math_random, math_sin,
      string_format
    = math.ceil, math.floor, math.pi, math.random, math.sin,
      string.format
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local api = _G[modname]

local skyseed = math_random(1, 65536)

local paws = {
	"catrealm_core_skypaw.png",
	"catrealm_core_skypaw.png^[transformR90",
	"catrealm_core_skypaw.png^[transformR180",
	"catrealm_core_skypaw.png^[transformR270",
	"catrealm_core_skypaw.png^[transformFX",
	"catrealm_core_skypaw.png^[transformFX^[transformR90",
	"catrealm_core_skypaw.png^[transformFX^[transformR180",
	"catrealm_core_skypaw.png^[transformFX^[transformR270",
}

local function getcolor(theta)
	local r = math_sin(theta + math_pi * 0/3) * 63 + 160
	local g = math_sin(theta + math_pi * 2/3) * 63 + 160
	local b = math_sin(theta + math_pi * 4/3) * 63 + 160
	return string_format("#%02x%02x%02x", math_ceil(r), math_ceil(g), math_ceil(b))
end
local function randpaws(seed)
	local rng = PcgRandom(seed + skyseed)
	local t = nodecore.tmod:combine(256, 256)
	for _ = 1, 10 do
		local u = nodecore.tmod(paws[rng:next(1, #paws)])
		:multiply(getcolor(rng:next(1, 65536) / 32768 * math_pi))
		t = t:layer(rng:next(0, 240), rng:next(0, 240), u)
	end
	return tostring(t)
end
local function calcalpha(offs)
	local t = nodecore.gametime / 20
	t = math_floor(t * 30 / math_pi) * math_pi / 30
	return math_floor(math_sin(t + math_pi * offs) * 127 + 128)
end

local function timedpaws(seed)
	return tostring(nodecore.tmod("catrealm_core_skybox.png")
		:add(nodecore.tmod(randpaws(seed + 10)):opacity(calcalpha(0/3)))
		:add(nodecore.tmod(randpaws(seed + 20)):opacity(calcalpha(2/3)))
		:add(nodecore.tmod(randpaws(seed + 30)):opacity(calcalpha(4/3))))
end

nodecore.register_playerstep({
		label = "catrealm skybox",
		priority = -100,
		action = function(player, data)
			if not api.in_catrealm(player:get_pos()) then return end
			data.sky = {
				base_color = "#333333",
				type = "skybox",
				textures = {
					timedpaws(1),
					timedpaws(2),
					timedpaws(3),
					timedpaws(4),
					timedpaws(5),
					timedpaws(6),
				},
				clouds = false,
			}
		end
	})
