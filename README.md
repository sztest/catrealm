This modpack adds a new cat-themed alternate dimension to NodeCore, along with some optional features.

Recipes are hinted at by custom Discoveries in the mods.

#### Core Mod:

Adds a new cat-themed alternate dimension that can be accessed from the normal vanilla world.  Surround an annealed lode cube with cats to create the portal.  Transport into the catrealm costs a cat prill each time.

Requires the NodeCore Cats mod.

#### Ores Mod:

Populate the catrealm with some exclusive ores.  Learn how to harvest, smelt, and craft with the new ores.  Create new and powerful tools.  Learn how to renew the ores and sustain a supply.

Optional, requires the CatRealm Core mod.

#### Sky Realm Access Mod:

Create a skyrealm portal within the catrealm, and access all vanilla resources in the catrealm.

Optional, requires the CatRealm Ores and SkyRealm mods.

---

*Designed by Warr1024 and Mearson*
